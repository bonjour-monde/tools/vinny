# Vinny



![](_img/vinny.png)


Vinny is like a vine growing on your picture and mimic it.    
It separate each color channel, apply the organic effect and output an svg with the 3 colors superposed.

## Getting Started    

### Prerequisites

This tool might work only on linux computer. :s

### Programs in use

In order to product the wanted result we've been using [AutoTrace](http://autotrace.sourceforge.net/index.html#features) a vectorisation software, python 3 (*svgutils*, *xml ElementTree*), bash and imagemagick.

## Use vinny

Use this command line :
```bash
./vinny mypicture.jpg
```
NB : don't forget to give the proper permission to the script.

This script also accept bulk procesing

```bash
./vinny *.jpg
```


### Edit the color

Colors are produce with the python script called *layering.py*.   
For each layer you can change the color of the path by changing the rgb value on the line 14, 20 ,26 :     
```python
child.set('style','stroke:rgb(201,47,22);fill:none;')   

```









![](_img/export_small.png)


## Author

[Bonjour Monde](http://bonjourmonde.net)

## License

[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)


